# -*- coding: utf-8 -*-
'''
Created on 2013-5-31

@author: CL.Lam
'''
from wtforms.widgets.core import Input, HTMLString
from wtforms.fields.core import DateField, Field, StringField

__all__ = ['MyDateField', ]

class DateWidget( Input ):
    input_type = 'text'

    def __call__( self, field, **kwargs ):
        kwargs.setdefault( 'id', field.id )
        kwargs.setdefault( 'type', self.input_type )
        if 'value' not in kwargs:
            kwargs['value'] = field._value()

        if 'class_' in kwargs : kwargs.pop( 'class_' )
        kwargs['class_'] = 'datepicker form-control'

        return HTMLString( '<input %s>' % self.html_params( name = field.name, **kwargs ) )


class MyDateField( StringField ):

    widget = DateWidget()


