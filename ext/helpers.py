# -*- coding: utf-8 -*-

"""WebHelpers used in label."""

from webhelpers import date, feedgenerator, html, number, misc, text
from reportlayout.model import DBSession

def get_options(obj, order_by_field="name", null_option=True):
    if null_option:
        return [("", ""),] + [(str(row.id), str(row)) for row in DBSession.query(obj).filter(obj.active == True).order_by(getattr(obj, order_by_field)).all()]
    else:
        return [(str(row.id), str(row)) for row in DBSession.query(obj).filter(obj.active == True).order_by(getattr(obj, order_by_field)).all()]


b = lambda v:"&nbsp;" if not v or v == 'Null' else v
pd = lambda v, len=10:"&nbsp;" if not v else str(v)[0:len]
pt = lambda v, len=19:"&nbsp;" if not v else str(v)[0:len]

jd = lambda v : json.dumps(v)
jl = lambda v : json.loads(v)


def tp(v):
    if not v : return "&nbsp;"
    return "<span class='tooltip' title='%s'>%s</span>" % (v,v)

#cound the day 
def cd(v):
    try:
        return (dt.now() - v).days
    except:
        traceback.print_exc()
        return ""
