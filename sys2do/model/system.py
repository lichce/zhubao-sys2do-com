# -*- coding: utf-8 -*-
'''
###########################################
#  Created on 2011-10-25
#  @author: cl.lam
#  Description:
###########################################
'''
import os
import traceback
from sqlalchemy import Column
from sqlalchemy.types import Integer, Text
from sys2do.model import DeclarativeBase

from sys2do.setting import UPLOAD_FOLDER_PREFIX
from interface import SysMixin, DBMixin
from sys2do.model import qry
from sys2do.util.image import getThumbnailSize


__all__ = ['SysDict', 'SysLog', 'SysFile']

#===============================================================================
# data table for system common function
#===============================================================================


_cache = None

class SysDict( DeclarativeBase ):
    __tablename__ = 'system_data_dictionary'


    id = Column( Integer, autoincrement = True, primary_key = True )
    type = Column( 'type', Text )
    name = Column( 'name', Text )
    value = Column( 'value', Text )
    remark = Column( 'remark', Text )
    active = Column( 'active', Integer, default = 0 )


    @classmethod
    def getValue( cls, type, name ):
        try:
            print '-*' * 10
            print cls.getCache()
            print '~*' * 10
            return cls.getCache()[type][name]
        except :
            traceback.print_exc()
            return None

    @classmethod
    def getName( cls, type, value ):
        try:
            for k, v in cls.getCache()[type].iteritems() :
                if v == value : return k
        except :
            traceback.print_exc()
        return None

    @classmethod
    def getValues( cls, type ):
        try:
            return cls.getCache()[type]
        except :
            traceback.print_exc()
            return {}

    @classmethod
    def getCache( cls ):
        global _cache
        if _cache is None:
            _cache = {}
            for record in qry( cls ).filter( cls.active == 0 ):
                if record.type not in _cache : _cache[record.type] = { record.name : record.value }
                else : _cache[record.type][record.name] = record.value
        return _cache



class SysLog( DeclarativeBase, SysMixin, DBMixin ):
    __tablename__ = 'system_log'

    id = Column( Integer, autoincrement = True, primary_key = True )
    refClz = Column( 'ref_clz', Text )
    type = Column( 'type', Text )
    refID = Column( 'ref_id', Integer )
    remark = Column( 'remark', Text )


class SysFile( DeclarativeBase, SysMixin ):
    __tablename__ = 'system_upload_file'

    id = Column( Integer, autoincrement = True, primary_key = True )
    name = Column( 'name', Text )
    path = Column( 'path', Text )
    url = Column( 'url', Text )
    size = Column( 'size', Integer, default = None )
    type = Column( 'type', Text )
    remark = Column( 'remark', Text )

    def __str__( self ): return self.name
    def __repr__( self ): return self.name
    def __unicode__( self ): return self.name

    @property
    def realPath( self ):
        return os.path.join( UPLOAD_FOLDER_PREFIX, self.path )

    def getThumbSize( self, des_width = 50, des_height = 50 ):
        return getThumbnailSize( self.realPath, des_width, des_height )
